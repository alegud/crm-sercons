$(document).ready(function() {
  $("#company-history").load('./scripts/company_history.php');
  $("#calls").load('./scripts/call_in_time.php');
	//Аякс отправка комментария
	//Документация: http://api.jquery.com/jquery.ajax/
	$("#comment").submit(function() {
		$.ajax({
			type: "POST",
			url: "./scripts/company_control.php",
			data: $("#comment").serialize()
		}).done(function() {
			$('#comment')[0].reset();
			$("#company-history").load('./scripts/company_history.php');
		});
		return false;
	});
	//Аякс отправка результат сделки
	//Документация: http://api.jquery.com/jquery.ajax/

	$("#dealsum").submit(function() {
		$.ajax({
			type: "POST",
			url: "./scripts/company_control.php",
			data: $("#dealsum").serialize()
		}).done(function() {
			$('#dealsum')[0].reset();
			$("#company-history").load('./scripts/company_history.php');
			$('#mydeal').modal('hide');
		});
		return false;
	});
		$("#calltime").submit(function() {
		$.ajax({
			type: "POST",
			url: "./scripts/company_control.php",
			data: $("#calltime").serialize()
		}).done(function() {
			$('#calltime')[0].reset();
			$("#company-history").load('./scripts/company_history.php');
			$("#calls").load('./scripts/call_in_time.php');
			$('#Callback').modal('hide');
		});
		return false;
	});
		$("vovrema").submit(function() {
		$.ajax({
			type: "POST",
			url: "./scripts/company_control.php",
			data: $("#vovrema").serialize()
		}).done(function() {
			alert("Thank you for your action!"); 
			$("#company-history").load('./scripts/company_history.php');
			$("#calls").load('./scripts/call_in_time.php');
		});
		return false;
	});
	    //Идентификатор элемента HTML (например: #datetimepicker1), для которого необходимо инициализировать виджет "Bootstrap datetimepicker"
    $('#datetimepicker1').datetimepicker(
      {language: 'ru'}
    );
});
