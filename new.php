<?php 
require_once './scripts/avtorizacia.php'; 
require_once './scripts/app_config.php';
require_once './scripts/database_connection.php';


if (array_key_exists('comment_text', $_REQUEST)) {
          $name_com = trim($_REQUEST['name_com']);
          $tel_com = trim($_REQUEST['tel_com']);
          $email_com = trim($_REQUEST['email_com']);
          $rubric_com = trim($_REQUEST['rubric_com']);
          $adres_com = trim($_REQUEST['adres_com']);
          $inn_com = trim($_REQUEST['inn_com']);
          $ogrn_com = trim($_REQUEST['ogrn_com']);
          $dop_com = trim($_REQUEST['dop_com']);
          $call_list_id = 0;
          $insert_sql = sprintf("INSERT INTO company_info (name_com, tel_com, email_com, rubric_com, adres_com, inn_com, ogrn_com, dop_com, call_list_id) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s')",
          mysql_real_escape_string($name_com),
          mysql_real_escape_string($tel_com),
          mysql_real_escape_string($email_com),
          mysql_real_escape_string($rubric_com),
          mysql_real_escape_string($adres_com),
          mysql_real_escape_string($inn_com),
          mysql_real_escape_string($ogrn_com),
          mysql_real_escape_string($dop_com),
          mysql_real_escape_string($call_list_id));


          // Insert the user into the database
          mysql_query($insert_sql)
            or die(mysql_error());
}
?>
<!DOCTYPE html>
<html lang="en">
      <head>
        <meta charset="UTF-8">
        <title>ЦРМ v0.1</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
          <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
          </head>
          <body>
           <div class="container">

            <!-- Static navbar -->
            <nav class="navbar navbar-default">
             <div class="container-fluid">
              <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">ЦРМ v0.1</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
             <ul class="nav navbar-nav">
              <li><a href="/index.php?id_com=1">Карточка компании</a></li>
              <li class="active"><a href="">Добавить новую компанию</a></li>
              <li><a href="">Выслано КП</a></li>
              <li><a href="">Отвал</a></li>
              <li><a href="">Воронка</a></li>
            </ul>
            <form class="navbar-form navbar-right">
              <div class="form-group">
               <input type="text" placeholder="Поиск" class="form-control">
             </div>
             <button type="submit" class="btn btn-success">Поиск</button>
           </form>
         </div><!--/.nav-collapse -->
       </div><!--/.container-fluid -->
      </nav>

      <!-- Example row of columns -->

      <div class="row">
       <div class="col-lg-8">
        <div class="alert alert-success" role="alert">
        <?php
        $select_company="SELECT * FROM company_info ORDER BY id_com DESC LIMIT 1";
        $companylist=mysql_query($select_company);
        $company_info2=mysql_fetch_array($companylist);
        $name_com1=$company_info2['name_com'];
        $id_com1=$company_info2['id_com'];
        ?>
        <p> Компания <strong><a href="./index.php?id_com=<?php echo $id_com1;?>" target="_blank"><?php echo $id_com1.'. '.$name_com1?> </a></strong> была добавлена </p>
        </div>
        <h2>Добавление новой компании</h2>
        <form id="company_new" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
         <div class="row">
          <div class="col-md-6">
            <label >Название организации:</label>
            <input type="text" class="form-control" placeholder="Название" name="name_com">
          </div>
          <div class="col-md-6"></div>
        </div>
        <p></p>
        <div class="row">
          <div class="col-md-4">
            <label >Телефоны:</label>
            <input type="text" class="form-control" placeholder="Телефоны" name="tel_com">
          </div>
          <div class="col-md-4">
            <label>Email:</label>
            <input type="text" class="form-control" placeholder="E-mail" name="email_com">
          </div>
          <div class="col-md-4">
            <label>Рубрика:</label>
            <input type="text" class="form-control" placeholder="Рубрика" name="rubric_com">
          </div>
        </div>
        <p></p>
        <div class="row">
          <div class="col-md-4">
           <label>Адрес:</label>
           <input type="text" class="form-control" placeholder="Адрес" name="adres_com">
         </div>
         <div class="col-md-4">
          <label>ИНН:</label>
          <input type="text" class="form-control" placeholder="ИНН" name="inn_com">
        </div>
        <div class="col-md-4">
          <label>ОГРН:</label>
          <input type="text" class="form-control" placeholder="ОГРН" name="comment_text" name="ogrn_com">
        </div>
      </div>
      <h3>Дополнительная информация</h3>
      <textarea class="form-control" rows="3" name="  dop_com"></textarea>
      <p></p>
      <div class="row">
       <div class="col-md-4"></div>
         <div class="col-md-4"></div>
         <div class="col-md-4"><button class="btn btn-primary" type="submit">Сохранить</button></div>
       </div>
      </form>

      </div> <!-- /container -->
      </div>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script type="text/javascript" src="js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Подключить скрипт moment-with-locales.min.js для работы с датами -->
      <script type="text/javascript" src="js/moment-with-locales.min.js"></script>
      <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
      <!-- Скрипт с выполняемыми функциями -->
      <script src="js/script.js"></script>
      </body>
      </html>