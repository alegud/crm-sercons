<?php

// Set up debug mode
define("DEBUG_MODE", true);

// Site root
define("SITE_ROOT", "/");

// Location of web files on host
define("HOST_WWW_ROOT", "/");
// Location of scripts on host
define("SCRIPTS_ROOT", "scripts/");

// Database connection constants
define("DATABASE_HOST", "localhost");
define("DATABASE_USERNAME", "asite_crm1");
define("DATABASE_PASSWORD", "grund1989");
define("DATABASE_NAME", "asite_crm1");

function debug_print($message) {
  if (DEBUG_MODE) {
    echo $message;
  }
}

function handle_error($user_error_message, $system_error_message) {
  session_start();
  $_SESSION['error_message'] = $user_error_message;
  $_SESSION['system_error_message'] = $system_error_message;
  header("Location: " . SITE_ROOT . "scripts/show_error.php"); 
  exit();
}

function get_web_path($file_system_path) {
  return str_replace($_SERVER['DOCUMENT_ROOT'], '', $file_system_path);
}
?>
