
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ЦРМ v0.1</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  </head>
  <body>
   <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">ЦРМ v0.1</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
              <li class="active"><a href="index.php?id_com=1">Карточка компании</a></li>
              <li><a href="/new.php">Добавить новую компанию</a></li>
              <li><a href="#">Выслано КП</a></li>
              <li><a href="#">Отвал</a></li>
              <li><a href="#">Воронка</a></li>
          </ul>
          <form class="navbar-form navbar-right">
              <div class="form-group">
                 <input type="text" placeholder="Поиск" class="form-control">
             </div>
             <button type="submit" class="btn btn-success">Поиск</button>
         </form>
     </div><!--/.nav-collapse -->
 </div><!--/.container-fluid -->
</nav>

<!-- Example row of columns -->
<div class="row">
 <div class="col-lg-8">
 

 <?php  include 'scripts/breadcrumps.php';?>
 <?php  include 'scripts/company_info.php';?>
      <form id="comment">
       <h3>Ваш комментарий</h3>
       <textarea class="form-control" rows="3" name="comment_text"></textarea>
       <input type="hidden" name="action_type" value="comment">
       <input type="hidden" name="id_com" value="<?php echo $id_com; ?>">
      <p></p>

      <div class="row">
       <div class="col-md-3"><button class="btn btn-primary" type="submit">Сохранить</button></div>
       </form>
       <div class="col-md-3"><button class="btn btn-primary" type="button" data-toggle="modal" data-target="#Callback">Запланировать дело</button></div>
       <div class="col-md-3"></div>
    </div>
    <p></p>
    <div class="row">
       <div class="col-md-3"></div>
       <div class="col-md-3"></div>
       <div class="col-md-3"></div>
    </div>
       
                       <!-- Modal Callback-->
        <div class="modal fade" id="Callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Перезвонить</h4>
              </div>
              <div class="modal-body">
                <form id="calltime">
                  <div class="form-group">
                  <label for="recipient-name" class="control-label">Дата время:</label>
                  <div class="input-group date" id="datetimepicker1">
                   <input type="text" class="form-control" name="calltime1"/><span class="input-group-addon">
                   <span class="glyphicon glyphicon-calendar"></span></span>
                  </div>
                    <label for="recipient-name" class="control-label" >Номер телефона:</label>
                    <input type="text" class="form-control" id="recipient-name" name="phone">
                    <label for="recipient-name" class="control-label">Цель звонка:</label>
                    <textarea class="form-control" rows="2" name="theme"></textarea>
                    <input type="hidden" name="action_type" value="callback">
                    <input type="hidden" name="id_com" value="<?php echo $id_com; ?>">
                </div>       
            </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </form>  
              </div>
            </div>
          </div>
        </div>

        <p></p>
      <div class="panel panel-default" id="company-history">
          
      </div>
  </div>
    <div class="col-lg-4" id="calls">
    </div>
  </div>



</div> <!-- /container -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Подключить скрипт moment-with-locales.min.js для работы с датами -->
  <script type="text/javascript" src="js/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
<!-- Скрипт с выполняемыми функциями -->
<script src="js/script.js"></script>
</body>
</html>